<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s1" />
        <signal name="s2" />
        <signal name="s3" />
        <signal name="XLXN_5" />
        <signal name="XLXN_8" />
        <signal name="V5" />
        <signal name="G0" />
        <signal name="XLXN_9" />
        <signal name="XLXN_6" />
        <signal name="F" />
        <port polarity="Input" name="s1" />
        <port polarity="Input" name="s2" />
        <port polarity="Input" name="s3" />
        <port polarity="Output" name="F" />
        <blockdef name="HCT138">
            <timestamp>2018-7-9T10:55:48</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-400" y2="-400" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <block symbolname="vcc" name="XLXI_4">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_5">
            <blockpin signalname="G0" name="G" />
        </block>
        <block symbolname="HCT138" name="U1">
            <blockpin signalname="G0" name="G_2A" />
            <blockpin signalname="G0" name="G_2B" />
            <blockpin signalname="V5" name="G" />
            <blockpin name="Y0_" />
            <blockpin signalname="XLXN_5" name="Y1_" />
            <blockpin signalname="XLXN_6" name="Y2_" />
            <blockpin name="Y3_" />
            <blockpin signalname="XLXN_8" name="Y4_" />
            <blockpin name="Y5_" />
            <blockpin name="Y6_" />
            <blockpin signalname="XLXN_9" name="Y7_" />
            <blockpin signalname="s3" name="C" />
            <blockpin signalname="s1" name="A" />
            <blockpin signalname="s2" name="B" />
        </block>
        <block symbolname="and4" name="XLXI_6">
            <blockpin signalname="XLXN_9" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_6" name="I2" />
            <blockpin signalname="XLXN_5" name="I3" />
            <blockpin signalname="F" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <branch name="s1">
            <wire x2="656" y1="192" y2="192" x1="144" />
        </branch>
        <branch name="s2">
            <wire x2="656" y1="272" y2="272" x1="144" />
        </branch>
        <branch name="s3">
            <wire x2="656" y1="352" y2="352" x1="144" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1328" y1="256" y2="256" x1="1040" />
            <wire x2="1328" y1="256" y2="320" x1="1328" />
            <wire x2="1344" y1="320" y2="320" x1="1328" />
            <wire x2="1376" y1="320" y2="320" x1="1344" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1344" y1="448" y2="448" x1="1040" />
            <wire x2="1376" y1="448" y2="448" x1="1344" />
        </branch>
        <branch name="V5">
            <attrtext style="alignment:HARD-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="592" type="branch" />
            <wire x2="448" y1="160" y2="592" x1="448" />
            <wire x2="656" y1="592" y2="592" x1="448" />
        </branch>
        <branch name="G0">
            <attrtext style="alignment:HARD-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="576" y="432" type="branch" />
            <wire x2="576" y1="432" y2="512" x1="576" />
            <wire x2="576" y1="512" y2="640" x1="576" />
            <wire x2="656" y1="512" y2="512" x1="576" />
            <wire x2="656" y1="432" y2="432" x1="576" />
        </branch>
        <instance x="512" y="768" name="XLXI_5" orien="R0" />
        <branch name="XLXN_9">
            <wire x2="1328" y1="640" y2="640" x1="1040" />
            <wire x2="1344" y1="512" y2="512" x1="1328" />
            <wire x2="1376" y1="512" y2="512" x1="1344" />
            <wire x2="1328" y1="512" y2="640" x1="1328" />
        </branch>
        <instance x="656" y="672" name="U1" orien="R0">
            <attrtext style="fontsize:58;fontname:Arial;textcolor:rgb(255,0,0)" attrname="InstName" x="144" y="-560" type="instance" />
        </instance>
        <branch name="XLXN_6">
            <wire x2="1184" y1="320" y2="320" x1="1040" />
            <wire x2="1184" y1="320" y2="384" x1="1184" />
            <wire x2="1344" y1="384" y2="384" x1="1184" />
            <wire x2="1376" y1="384" y2="384" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="144" y="192" name="s1" orien="R180" />
        <iomarker fontsize="28" x="144" y="272" name="s2" orien="R180" />
        <iomarker fontsize="28" x="144" y="352" name="s3" orien="R180" />
        <instance x="384" y="160" name="XLXI_4" orien="R0" />
        <instance x="1376" y="576" name="XLXI_6" orien="R0" />
        <branch name="F">
            <wire x2="1664" y1="416" y2="416" x1="1632" />
        </branch>
        <iomarker fontsize="28" x="1664" y="416" name="F" orien="R0" />
    </sheet>
</drawing>