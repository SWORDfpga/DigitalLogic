`timescale 1ns / 1ps

module    Top_HTC138(input clk200_p,
							input clk200_n,
							input wire[15:0]SW, 
							input A,B,C,G,G_2A,G_2B,
							output wire led_clk,
							output wire led_sout,
							output wire led_clrn,
							output wire LED_PEN,
							output [7:0] Y,
							output Buzzer
							);
							
wire[31:0]Div,PD;
wire [7:0]Y;
wire ny0,ny1,ny2,ny3,ny4,ny5,ny6,ny7;

wire clk200m;
IBUFDS inst_clk(
.I(clk200_p),
.IB(clk200_n),
.O(clk200m)
);
reg clk_100mhz;
always@(posedge clk200m)
	clk_100mhz<=~clk_100mhz;
	assign clk = clk_100mhz;
	assign Buzzer = 1'b1;
	
	HCT138	M5(A,B,C,G,G_2A,G_2B,ny0,ny1,ny2,ny3,ny4,ny5,ny6,ny7);
	assign Y = ~{ny7,ny6,ny5,ny4,ny3,ny2,ny1,ny0};


	clkdiv		U9(.clk(clk),
						.clkdiv(Div)
						);

	SPLIO			U7(.clk(clk),
						.rst(rst),
						.Start(Div[20]),
						.EN(1'b1),
						.P_Data({24'hFFFFFF,Y}),
						.LED(),
						.led_clk(led_clk),
						.led_sout(led_sout),
						.led_clrn(led_clrn),
						.LED_PEN(LED_PEN),
						.GPIOf0()
						);	

endmodule
