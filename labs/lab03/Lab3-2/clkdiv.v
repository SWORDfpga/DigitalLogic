`timescale 1ns / 1ps

module clkdiv(
    input clk,
    input rst,
    output reg [31:0] clkdiv
);

always@(posedge clk) begin
	if(rst) clkdiv<=0;
	else clkdiv<=clkdiv+1;
end

endmodule
