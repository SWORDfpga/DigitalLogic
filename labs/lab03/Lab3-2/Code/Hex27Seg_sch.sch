<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="AN(3:0)" />
        <signal name="Hex(0)" />
        <signal name="Hex(1)" />
        <signal name="Hex(2)" />
        <signal name="Hex(3)" />
        <signal name="SEGMENT(7)" />
        <signal name="SEGMENT(6)" />
        <signal name="SEGMENT(5)" />
        <signal name="SEGMENT(4)" />
        <signal name="SEGMENT(3)" />
        <signal name="SEGMENT(2)" />
        <signal name="SEGMENT(1)" />
        <signal name="SEGMENT(0)" />
        <signal name="SEGMENT(7:0)" />
        <signal name="Buzzer" />
        <signal name="V5" />
        <signal name="clk200_p" />
        <signal name="clk200_n" />
        <signal name="XLXN_12" />
        <signal name="clkdiv(31:0)" />
        <signal name="Hex(3:0)" />
        <signal name="clkdiv(18:17)" />
        <signal name="XLXN_22" />
        <signal name="RSTN" />
        <signal name="SW(7:0)" />
        <signal name="SW(3:0)" />
        <signal name="SW(7:4)" />
        <signal name="XLXN_29" />
        <signal name="XLXN_31" />
        <signal name="G0,V5,G0,G0,G0,G0,V5,V5,G0,G0,V5,G0,G0,G0,G0,V5" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Output" name="SEGMENT(7:0)" />
        <port polarity="Output" name="Buzzer" />
        <port polarity="Input" name="clk200_p" />
        <port polarity="Input" name="clk200_n" />
        <port polarity="Input" name="RSTN" />
        <port polarity="Input" name="SW(7:0)" />
        <blockdef name="MC14495_ZJU">
            <timestamp>2015-10-25T6:35:25</timestamp>
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-336" y2="-336" x1="64" />
            <rect width="192" x="64" y="-404" height="404" />
            <line x2="320" y1="-32" y2="-32" x1="256" />
            <line x2="320" y1="-80" y2="-80" x1="256" />
            <line x2="320" y1="-128" y2="-128" x1="256" />
            <line x2="320" y1="-176" y2="-176" x1="256" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-272" y2="-272" x1="256" />
            <line x2="320" y1="-320" y2="-320" x1="256" />
            <line x2="320" y1="-368" y2="-368" x1="256" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="clkdiv">
            <timestamp>2018-7-11T19:28:45</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="ibufds">
            <timestamp>2008-10-8T10:10:10</timestamp>
            <line x2="64" y1="-48" y2="-48" x1="0" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <circle r="8" cx="56" cy="-16" />
            <line x2="48" y1="-16" y2="-16" x1="0" />
        </blockdef>
        <blockdef name="dispsync">
            <timestamp>2018-7-11T19:28:57</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="MC14495_ZJU" name="M1">
            <blockpin signalname="Hex(0)" name="D0" />
            <blockpin signalname="Hex(1)" name="D1" />
            <blockpin signalname="Hex(2)" name="D2" />
            <blockpin signalname="Hex(3)" name="D3" />
            <blockpin signalname="SEGMENT(7)" name="p" />
            <blockpin signalname="SEGMENT(6)" name="g" />
            <blockpin signalname="SEGMENT(5)" name="f" />
            <blockpin signalname="SEGMENT(4)" name="e" />
            <blockpin signalname="SEGMENT(3)" name="d" />
            <blockpin signalname="SEGMENT(2)" name="c" />
            <blockpin signalname="SEGMENT(1)" name="b" />
            <blockpin signalname="SEGMENT(0)" name="a" />
            <blockpin signalname="XLXN_29" name="point" />
            <blockpin signalname="XLXN_31" name="LE" />
        </block>
        <block symbolname="buf" name="XLXI_41">
            <blockpin signalname="V5" name="I" />
            <blockpin signalname="Buzzer" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_60">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="clkdiv" name="M0">
            <blockpin signalname="XLXN_12" name="clk" />
            <blockpin signalname="XLXN_22" name="rst" />
            <blockpin signalname="clkdiv(31:0)" name="clkdiv(31:0)" />
        </block>
        <block symbolname="ibufds" name="XLXI_62">
            <blockpin signalname="clk200_p" name="I" />
            <blockpin signalname="clk200_n" name="IB" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="dispsync" name="M2">
            <blockpin signalname="G0,V5,G0,G0,G0,G0,V5,V5,G0,G0,V5,G0,G0,G0,G0,V5" name="Hexs(15:0)" />
            <blockpin signalname="clkdiv(18:17)" name="Scan(1:0)" />
            <blockpin signalname="SW(3:0)" name="point(3:0)" />
            <blockpin signalname="SW(7:4)" name="blink(3:0)" />
            <blockpin signalname="XLXN_29" name="p" />
            <blockpin signalname="XLXN_31" name="LE" />
            <blockpin signalname="Hex(3:0)" name="Hex(3:0)" />
            <blockpin signalname="AN(3:0)" name="AN(3:0)" />
        </block>
        <block symbolname="inv" name="XLXI_64">
            <blockpin signalname="RSTN" name="I" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <branch name="AN(3:0)">
            <wire x2="1168" y1="832" y2="832" x1="544" />
            <wire x2="1168" y1="832" y2="976" x1="1168" />
            <wire x2="1360" y1="976" y2="976" x1="1168" />
        </branch>
        <iomarker fontsize="28" x="1360" y="976" name="AN(3:0)" orien="R0" />
        <branch name="Hex(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="560" y="464" type="branch" />
            <wire x2="560" y1="464" y2="464" x1="496" />
            <wire x2="608" y1="464" y2="464" x1="560" />
        </branch>
        <branch name="Hex(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="560" y="416" type="branch" />
            <wire x2="560" y1="416" y2="416" x1="496" />
            <wire x2="608" y1="416" y2="416" x1="560" />
        </branch>
        <branch name="Hex(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="560" y="368" type="branch" />
            <wire x2="560" y1="368" y2="368" x1="496" />
            <wire x2="608" y1="368" y2="368" x1="560" />
        </branch>
        <branch name="Hex(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="560" y="320" type="branch" />
            <wire x2="560" y1="320" y2="320" x1="496" />
            <wire x2="608" y1="320" y2="320" x1="560" />
        </branch>
        <instance x="608" y="656" name="M1" orien="R0">
            <attrtext style="fontsize:58;fontname:Arial" attrname="InstName" x="112" y="-432" type="instance" />
        </instance>
        <bustap x2="1072" y1="624" y2="624" x1="1168" />
        <bustap x2="1072" y1="576" y2="576" x1="1168" />
        <bustap x2="1072" y1="528" y2="528" x1="1168" />
        <bustap x2="1072" y1="480" y2="480" x1="1168" />
        <bustap x2="1072" y1="432" y2="432" x1="1168" />
        <bustap x2="1072" y1="384" y2="384" x1="1168" />
        <bustap x2="1072" y1="336" y2="336" x1="1168" />
        <bustap x2="1072" y1="288" y2="288" x1="1168" />
        <branch name="SEGMENT(7)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="624" type="branch" />
            <wire x2="992" y1="624" y2="624" x1="928" />
            <wire x2="1072" y1="624" y2="624" x1="992" />
        </branch>
        <branch name="SEGMENT(6)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="576" type="branch" />
            <wire x2="992" y1="576" y2="576" x1="928" />
            <wire x2="1072" y1="576" y2="576" x1="992" />
        </branch>
        <branch name="SEGMENT(5)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="528" type="branch" />
            <wire x2="992" y1="528" y2="528" x1="928" />
            <wire x2="1072" y1="528" y2="528" x1="992" />
        </branch>
        <branch name="SEGMENT(4)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="480" type="branch" />
            <wire x2="992" y1="480" y2="480" x1="928" />
            <wire x2="1072" y1="480" y2="480" x1="992" />
        </branch>
        <branch name="SEGMENT(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="432" type="branch" />
            <wire x2="992" y1="432" y2="432" x1="928" />
            <wire x2="1072" y1="432" y2="432" x1="992" />
        </branch>
        <branch name="SEGMENT(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="384" type="branch" />
            <wire x2="992" y1="384" y2="384" x1="928" />
            <wire x2="1072" y1="384" y2="384" x1="992" />
        </branch>
        <branch name="SEGMENT(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="336" type="branch" />
            <wire x2="992" y1="336" y2="336" x1="928" />
            <wire x2="1072" y1="336" y2="336" x1="992" />
        </branch>
        <branch name="SEGMENT(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="288" type="branch" />
            <wire x2="992" y1="288" y2="288" x1="928" />
            <wire x2="1072" y1="288" y2="288" x1="992" />
        </branch>
        <branch name="SEGMENT(7:0)">
            <wire x2="1168" y1="288" y2="336" x1="1168" />
            <wire x2="1168" y1="336" y2="384" x1="1168" />
            <wire x2="1168" y1="384" y2="432" x1="1168" />
            <wire x2="1168" y1="432" y2="448" x1="1168" />
            <wire x2="1168" y1="448" y2="480" x1="1168" />
            <wire x2="1168" y1="480" y2="528" x1="1168" />
            <wire x2="1168" y1="528" y2="576" x1="1168" />
            <wire x2="1168" y1="576" y2="624" x1="1168" />
            <wire x2="1328" y1="448" y2="448" x1="1168" />
        </branch>
        <iomarker fontsize="28" x="1328" y="448" name="SEGMENT(7:0)" orien="R0" />
        <instance x="1072" y="816" name="XLXI_41" orien="R0" />
        <branch name="Buzzer">
            <wire x2="1376" y1="784" y2="784" x1="1296" />
        </branch>
        <branch name="V5">
            <wire x2="1072" y1="784" y2="784" x1="880" />
        </branch>
        <instance x="816" y="784" name="XLXI_60" orien="R0" />
        <iomarker fontsize="28" x="1376" y="784" name="Buzzer" orien="R0" />
        <instance x="400" y="160" name="M0" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="64" y="-160" type="instance" />
        </instance>
        <instance x="160" y="96" name="XLXI_62" orien="R0" />
        <branch name="clk200_p">
            <wire x2="160" y1="48" y2="48" x1="144" />
        </branch>
        <branch name="clk200_n">
            <wire x2="160" y1="80" y2="80" x1="144" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="400" y1="64" y2="64" x1="384" />
        </branch>
        <iomarker fontsize="28" x="144" y="48" name="clk200_p" orien="R180" />
        <iomarker fontsize="28" x="144" y="80" name="clk200_n" orien="R180" />
        <branch name="clkdiv(31:0)">
            <wire x2="832" y1="176" y2="176" x1="64" />
            <wire x2="64" y1="176" y2="288" x1="64" />
            <wire x2="832" y1="64" y2="64" x1="784" />
            <wire x2="832" y1="64" y2="176" x1="832" />
        </branch>
        <bustap x2="160" y1="288" y2="288" x1="64" />
        <instance x="160" y="864" name="M2" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="48" y="-288" type="instance" />
        </instance>
        <branch name="Hex(3:0)">
            <wire x2="400" y1="320" y2="368" x1="400" />
            <wire x2="400" y1="368" y2="416" x1="400" />
            <wire x2="400" y1="416" y2="464" x1="400" />
            <wire x2="400" y1="464" y2="528" x1="400" />
            <wire x2="576" y1="528" y2="528" x1="400" />
            <wire x2="576" y1="528" y2="768" x1="576" />
            <wire x2="576" y1="768" y2="768" x1="544" />
        </branch>
        <bustap x2="496" y1="320" y2="320" x1="400" />
        <bustap x2="496" y1="368" y2="368" x1="400" />
        <bustap x2="496" y1="416" y2="416" x1="400" />
        <bustap x2="496" y1="464" y2="464" x1="400" />
        <branch name="clkdiv(18:17)">
            <wire x2="160" y1="288" y2="288" x1="128" />
            <wire x2="128" y1="288" y2="704" x1="128" />
            <wire x2="160" y1="704" y2="704" x1="128" />
        </branch>
        <instance x="144" y="160" name="XLXI_64" orien="R0" />
        <branch name="XLXN_22">
            <wire x2="400" y1="128" y2="128" x1="368" />
        </branch>
        <branch name="RSTN">
            <wire x2="144" y1="128" y2="128" x1="128" />
        </branch>
        <iomarker fontsize="28" x="128" y="128" name="RSTN" orien="R180" />
        <branch name="SW(7:0)">
            <wire x2="16" y1="768" y2="832" x1="16" />
            <wire x2="16" y1="832" y2="912" x1="16" />
            <wire x2="144" y1="912" y2="912" x1="16" />
            <wire x2="144" y1="912" y2="1056" x1="144" />
        </branch>
        <iomarker fontsize="28" x="144" y="1056" name="SW(7:0)" orien="R180" />
        <bustap x2="112" y1="768" y2="768" x1="16" />
        <bustap x2="112" y1="832" y2="832" x1="16" />
        <branch name="SW(3:0)">
            <wire x2="160" y1="768" y2="768" x1="112" />
        </branch>
        <branch name="SW(7:4)">
            <wire x2="160" y1="832" y2="832" x1="112" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="560" y1="640" y2="640" x1="544" />
            <wire x2="560" y1="624" y2="640" x1="560" />
            <wire x2="608" y1="624" y2="624" x1="560" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="592" y1="704" y2="704" x1="544" />
            <wire x2="608" y1="560" y2="560" x1="592" />
            <wire x2="592" y1="560" y2="704" x1="592" />
        </branch>
        <branch name="G0,V5,G0,G0,G0,G0,V5,V5,G0,G0,V5,G0,G0,G0,G0,V5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="336" y="976" type="branch" />
            <wire x2="160" y1="640" y2="640" x1="80" />
            <wire x2="80" y1="640" y2="976" x1="80" />
            <wire x2="336" y1="976" y2="976" x1="80" />
        </branch>
    </sheet>
</drawing>