`timescale 1ns / 1ps

module lamp_Controler(
    input clk200_p,
    input clk200_n,
    input S1,
    input S2,
    input S3,
    output reg F
    );
parameter COUNTER=28;

wire clk;
IBUFDS inst_clk(
.I(clk200_p),
.IB(clk200_n),
.O(clk)
);

reg [COUNTER-1:0] count;
`define Dleay 28'hFFFFFFFF
wire w;
initial begin
	count <= `Dleay;
	F = 0;
end

assign w={S1 || S2 || S3};
always@*
	if(w||count<`Dleay)F=0;
	else F=1;

always@(posedge clk)
	if(w||count < `Dleay)
		count <= count+1;
	else count <= count;

endmodule
