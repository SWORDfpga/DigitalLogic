`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:35:13 07/08/2018
// Design Name:   lamp_Controler
// Module Name:   C:/thegit/20180705/DigitalLogicDesign/labs/lab01/Lab1-2/lamp_test.v
// Project Name:  Lab1-2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: lamp_Controler
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module lamp_test;

	// Inputs
	reg clk200_p;
	reg clk200_n;
	reg S1;
	reg S2;
	reg S3;

	// Outputs
	wire F;

	// Instantiate the Unit Under Test (UUT)
	lamp_Controler uut (
		.clk200_p(clk200_p), 
		.clk200_n(clk200_n), 
		.S1(S1), 
		.S2(S2), 
		.S3(S3), 
		.F(F)
	);

	initial begin
		// Initialize Inputs
		clk200_p = 0;
		clk200_n = 0;
		S1 = 0;
		S2 = 0;
		S3 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

