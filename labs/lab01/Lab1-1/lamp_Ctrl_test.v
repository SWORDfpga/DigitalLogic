// Verilog test fixture created from schematic /home/hawk/3workspace/lesson/Lab1-1/lamp_Ctrl.sch - Fri Jul  6 11:29:33 2018

`timescale 1ns / 1ps

module lamp_Ctrl_lamp_Ctrl_sch_tb();

// Inputs
   reg S1;
   reg S2;
   reg S3;

// Output
   wire F;

// Bidirs

// Instantiate the UUT
   lamp_Ctrl UUT (
		.S1(S1), 
		.S2(S2), 
		.S3(S3), 
		.F(F)
   );
// Initialize Inputs
//   `ifdef auto_init
	integer i;
       initial begin
		S1 = 0;
		S2 = 0;
		S3 = 0;
		for(i=0;i<=7;i=i+1)begin
		{S1,S2,S3}<=i;
		#20;
		end
	end
//   `endif
endmodule
