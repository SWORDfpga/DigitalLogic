<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="S1" />
        <signal name="XLXN_4" />
        <signal name="XLXN_6" />
        <signal name="XLXN_8" />
        <signal name="S2" />
        <signal name="S3" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="F" />
        <port polarity="Input" name="S1" />
        <port polarity="Input" name="S2" />
        <port polarity="Input" name="S3" />
        <port polarity="Output" name="F" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="S1" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="S2" name="I" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_3">
            <blockpin signalname="S3" name="I" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_4">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="S1" name="I2" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_5">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="S2" name="I1" />
            <blockpin signalname="XLXN_4" name="I2" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_6">
            <blockpin signalname="S3" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_4" name="I2" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_7">
            <blockpin signalname="S1" name="I0" />
            <blockpin signalname="S3" name="I1" />
            <blockpin signalname="S2" name="I2" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_8">
            <blockpin signalname="XLXN_19" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_16" name="I2" />
            <blockpin signalname="XLXN_18" name="I3" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="XLXN_20" name="I" />
            <blockpin signalname="F" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="816" y="544" name="XLXI_1" orien="R0" />
        <instance x="816" y="768" name="XLXI_2" orien="R0" />
        <instance x="816" y="976" name="XLXI_3" orien="R0" />
        <branch name="S1">
            <wire x2="528" y1="512" y2="512" x1="416" />
            <wire x2="816" y1="512" y2="512" x1="528" />
            <wire x2="528" y1="512" y2="1264" x1="528" />
            <wire x2="544" y1="1264" y2="1264" x1="528" />
            <wire x2="1328" y1="1264" y2="1264" x1="544" />
            <wire x2="1328" y1="448" y2="448" x1="528" />
            <wire x2="528" y1="448" y2="512" x1="528" />
        </branch>
        <instance x="1328" y="640" name="XLXI_4" orien="R0" />
        <instance x="1328" y="864" name="XLXI_5" orien="R0" />
        <instance x="1328" y="1072" name="XLXI_6" orien="R0" />
        <instance x="1328" y="1328" name="XLXI_7" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1120" y1="512" y2="512" x1="1040" />
            <wire x2="1120" y1="512" y2="672" x1="1120" />
            <wire x2="1328" y1="672" y2="672" x1="1120" />
            <wire x2="1120" y1="672" y2="880" x1="1120" />
            <wire x2="1328" y1="880" y2="880" x1="1120" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1152" y1="736" y2="736" x1="1040" />
            <wire x2="1152" y1="736" y2="944" x1="1152" />
            <wire x2="1328" y1="944" y2="944" x1="1152" />
            <wire x2="1328" y1="512" y2="512" x1="1152" />
            <wire x2="1152" y1="512" y2="736" x1="1152" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1040" y1="800" y2="944" x1="1040" />
            <wire x2="1216" y1="800" y2="800" x1="1040" />
            <wire x2="1328" y1="800" y2="800" x1="1216" />
            <wire x2="1328" y1="576" y2="576" x1="1216" />
            <wire x2="1216" y1="576" y2="800" x1="1216" />
        </branch>
        <branch name="S2">
            <wire x2="560" y1="832" y2="832" x1="416" />
            <wire x2="1280" y1="832" y2="832" x1="560" />
            <wire x2="560" y1="832" y2="1136" x1="560" />
            <wire x2="640" y1="1136" y2="1136" x1="560" />
            <wire x2="1328" y1="1136" y2="1136" x1="640" />
            <wire x2="816" y1="736" y2="736" x1="560" />
            <wire x2="560" y1="736" y2="832" x1="560" />
            <wire x2="1328" y1="736" y2="736" x1="1280" />
            <wire x2="1280" y1="736" y2="832" x1="1280" />
        </branch>
        <branch name="S3">
            <wire x2="592" y1="1008" y2="1008" x1="416" />
            <wire x2="608" y1="1008" y2="1008" x1="592" />
            <wire x2="640" y1="1008" y2="1008" x1="608" />
            <wire x2="1328" y1="1008" y2="1008" x1="640" />
            <wire x2="592" y1="1008" y2="1200" x1="592" />
            <wire x2="624" y1="1200" y2="1200" x1="592" />
            <wire x2="1328" y1="1200" y2="1200" x1="624" />
            <wire x2="816" y1="944" y2="944" x1="592" />
            <wire x2="592" y1="944" y2="1008" x1="592" />
        </branch>
        <instance x="1744" y="1008" name="XLXI_8" orien="R0" />
        <branch name="XLXN_16">
            <wire x2="1632" y1="736" y2="736" x1="1584" />
            <wire x2="1632" y1="736" y2="816" x1="1632" />
            <wire x2="1744" y1="816" y2="816" x1="1632" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="1632" y1="944" y2="944" x1="1584" />
            <wire x2="1632" y1="880" y2="944" x1="1632" />
            <wire x2="1744" y1="880" y2="880" x1="1632" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1680" y1="512" y2="512" x1="1584" />
            <wire x2="1680" y1="512" y2="528" x1="1680" />
            <wire x2="1680" y1="528" y2="752" x1="1680" />
            <wire x2="1744" y1="752" y2="752" x1="1680" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="1680" y1="1200" y2="1200" x1="1584" />
            <wire x2="1744" y1="944" y2="944" x1="1680" />
            <wire x2="1680" y1="944" y2="1184" x1="1680" />
            <wire x2="1680" y1="1184" y2="1200" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="416" y="512" name="S1" orien="R180" />
        <iomarker fontsize="28" x="416" y="832" name="S2" orien="R180" />
        <iomarker fontsize="28" x="416" y="1008" name="S3" orien="R180" />
        <instance x="2048" y="880" name="XLXI_9" orien="R0" />
        <branch name="XLXN_20">
            <wire x2="2048" y1="848" y2="848" x1="2000" />
        </branch>
        <branch name="F">
            <wire x2="2320" y1="848" y2="848" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2320" y="848" name="F" orien="R0" />
    </sheet>
</drawing>